var crypto = require('crypto');
var mongoose = require('lib/mongoose');
var AuthError = require('error').AuthError;
var async = require('async');
var Schema = mongoose.Schema;

var schema = new Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    hashedPassword: {
        type: String,
        required: true
    },
    salt: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    }
});

schema.methods.encryptPassword = function(password) {
  return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
};

schema.virtual('password').
    set(function(password) {
        this._plainPassword = password;
        this.salt = Math.random() + '';
        this.hashedPassword = this.encryptPassword(password);
    }).
    get(function() {
        return this._plainPassword;
    });

schema.methods.checkPassword = function (password) {
    return this.encryptPassword(password) === this.hashedPassword;
};

schema.statics.authorize = function (username, password, mainCallback) {
    var User = this;

    async.waterfall([
        callback => {
            User.findOne({username: username}, callback);
        },
        (user, callback) => {
            if (user) {
                if (user.checkPassword(password)) {
                    callback(null, user);
                } else {
                    callback(new AuthError('Wrong password'));
                }
            } else {
                user = new User({username: username, password: password});
                user.save(err => {
                    if (err) return callback(err);
                    callback(null, user);
                });
            }
        }
    ], mainCallback);
};
module.exports = mongoose.model('User', schema);