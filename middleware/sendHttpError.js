/**
 * Created by Artur on 10.07.2016.
 */
module.exports = function (req, res, next) {
    res.sendHttpError = function (error) {
        res.status(error.status);
        if (res.req.headers['x-requested-with'] == 'XMLHttpRequest') {
            res.json({
                error: error.message, 
                status: error.status
            });
        } else {
            res.render('error', {error: error});
        }
    };
    next();
};