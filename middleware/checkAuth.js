/**
 * Created by Artur on 17.07.2016.
 */
var HttpError = require('error').HttpError;

module.exports = function (req, res, next) {
	if (!req.session.user) {
		return next(new HttpError(401, 'Not authorized'));
	}

	next();
};