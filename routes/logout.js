'use strict';
var EventEmitter = require("events").EventEmitter;
var ee = new EventEmitter();
var sessionStore = require('lib/sessionStore');

exports.post = function (req, res, next) {
	var sid = req.session.id;
	var io = req.app.get('io');

	ee.on('session:reload', function (sid) {
		var clients = io.sockets.clients();

		for (let socketId in clients.connected) {
			let socket = clients.connected[socketId];
			if (socket.request.signedCookies.sid != sid) continue;

			sessionStore.get(sid, (err, session) => {
				if (err || !session) {
					socket.emit('logout');
					socket.disconnect();
					return;
				}

				socket.request.signedCookies.sid = session;
			});
		}

	});
	req.session.destroy(function(err) {
		ee.emit('session:reload', sid);

		if (err) next(err);
		res.redirect('/');
	});

};