var mongoose = require('lib/mongoose');
var async = require('async');

async.series([
    open,
    dropDatabase,
    requireModels,
    createUsers
], function (err) {
    console.log(arguments);
    mongoose.disconnect();
});

function open(callback) {
    mongoose.connection.on('open', callback);
}

function dropDatabase(callback) {
    var db = mongoose.connection.db;
    db.dropDatabase(callback);
}

function requireModels(callback) {
    require('models/user');

    async.each(Object.keys(mongoose.models), function (modelName, callback) {
        mongoose.models[modelName].ensureIndexes(callback);
    }, callback);
}

function createUsers(callback) {
    var users = [
        {username: "Greg", password: "123123"},
        {username: "Martin", password: "qweqwe"},
        {username: "Ronaldini", password: "zxczxczxc"}
    ];

    async.each(users, function (userData, callback) {
        var user = new mongoose.models.User(userData);
        user.save(callback);
    }, callback);
}

// function getPromise(entity) {
//     return new Promise(function (resolve, reject) {
//         entity.save(function (err, data) {
//             if (err) {
//                 reject(err);
//             }
//             resolve(data);
//         })
//     });
// }

//     Promise.all([
//         getPromise(greg),
//         getPromise(alisa),
//         getPromise(ronaldini)
//     ]).then(
//         result => {
//             console.log(result);
//             mongoose.disconnect();
//         },
//         error => {
//             console.log(error);
//             mongoose.disconnect();
//         }
//     );
//
//