'use strict';
var ioServer = require('socket.io');
var sessionStore = require('lib/sessionStore');
var cookieParser = require('socket.io-cookie-parser');
var config = require('config');
var User = require('models/user');
var HttpError = require('error').HttpError;

var io = new ioServer();

function socketAuth(socket) {
	return new Promise((resolve, reject) => {
		var sessId = socket.request.signedCookies.sid;
		sessionStore.get(sessId, (err, session) => {
			if (arguments.length == 0 || err) {
				reject(err || 'No session data');
			} else {
				resolve(session);
			}
		});
	});
}

function userLoad(session) {
	return new Promise((resolve, reject) => {
		if (!session.user) reject('No user data');

		User.findById(session.user, function(err, user) {
			if (err) reject(err);
			resolve(user);
		});
	});
}

module.exports = function (server) {
	io.listen(server, {
		origins: 'localhost:* www.localhost:*',
	});
	io.use(cookieParser(config.get('session:secret')));
	io.use(function(socket, next) {
		socketAuth(socket)
			.then(session => {
				return userLoad(session);
			})
			.then(user => {
				if (!user) {
					next(new HttpError('Anonymous user. no sess data'));
				}
				socket.handshake.user = user;
				next();
			})
			.catch(err => {
				if (typeof  err == 'string') {
					next(new HttpError(err));
				}
				next(err);
			});
	});

	io.on('connection', function (socket) {
		var username = socket.handshake.user.get('username');

		socket.broadcast.emit('join', username);

		socket.on('message', (data, callback) => {
			socket.broadcast.emit('message', username, data);
			callback('OK');
		});

		socket.on('disconnect', () => {
			socket.broadcast.emit('leave', username);
		});
	});

	return io;
};
