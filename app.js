'use strict';

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var morgan = require('morgan');
var engine = require('ejs-mate');

var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');

var errorHandler = require('errorhandler');
var HttpError = require('error').HttpError;
var http = require('http');
var log = require('lib/log')(module);
var mongoose = require('lib/mongoose');
var config = require('config');

var app = express();

app.engine('ejs', engine);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(morgan('dev'));
app.use(bodyParser.json()); // req.body
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser()); // req.cookies
app.use(session(
  Object.assign(
      config.get('session'),
      {store: require('lib/sessionStore')}
  )
));

app.use(express.static(path.join(__dirname, 'public')));
app.use(require('middleware/sendHttpError'));
app.use(require('middleware/loadUser'));

require('./routes/index')(app);
//  error handler
app.use(function(err, req, res, next) {
  if (typeof err == "number") {
    err = new HttpError(err);
  }

  if (err instanceof HttpError) {
    res.sendHttpError(err);
  } else {
    if (app.get('env') === 'development') {
      errorHandler()(err, req, res, next);
    } else {
      log.error(err);
      err = new HttpError(500);
      res.sendHttpError(err);
    }
  }
});

// var listener = app.listen(config.get('port'), () => {
//   log.info('Listening on port ' + listener.address().port);
// });
var server = require('http').createServer(app);
var listener = server.listen(config.get('port'), () => {
  log.info('Listening on port ' + listener.address().port);
});

var io = require('./socket')(server);
app.set('io', io);
