'use strict';

var util = require('util');
var http = require('http');

class HttpError extends Error {
    constructor(status, message) {
        super(message);
        this.name = this.constructor.name;
        this.message = message || http.STATUS_CODES[status] || "Error";
        this.status = status;
        if (typeof Error.captureStackTrace === 'function') {
            Error.captureStackTrace(this, this.constructor);
        } else {
            this.stack = (new Error(message)).stack;
        }
    }
}

exports.HttpError = HttpError;

class AuthError extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
        this.message = message;
        
        if (typeof Error.captureStackTrace === 'function') {
            Error.captureStackTrace(this, this.constructor);
        } else {
            this.stack = (new Error(message)).stack;
        }
    }
}

exports.AuthError = AuthError;